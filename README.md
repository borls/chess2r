An example universal JS application written with the 2R stack, *Re*act + *Re*dux.

This project is useful for:
 - seeing how to build a Universal Javascript application
 - understanding how to handle asyncronousity in Redux action creators
 - building your own Redux powered application
 - forking so that you can build your own 2R app!

### Main Features

 - Universal (Isomorphic) Javascript

### Setup
 - Clone the repo `git clone git@github.com:borls/chess2r.git`
 - Make sure you are using Node v6+
 - Run `npm install`
 - If your local environment is not reflected by `config/default.json`, then add a file at `config/local.json` to provide local customisation.

### Running Dev Server

On Linux/OSX: `npm start`

On Windows: `npm run start:win`

Go to http://localhost:3000

### Tech Used

| **Tech** | **Description** | **Version** |
| ---------|-----------------|-------------|
| [React](https://facebook.github.io/react/) | View layer | 15.0.2 |
| [React Router](https://github.com/reactjs/react-router) | Universal routing | 2.4.0 |
| [Redux](http://redux.js.org/) | State management | 3.5.0 |
| [RethinkDB](http://www.rethinkdb.com) | Persistance layer | 2.3.1 |
| [Express](http://expressjs.com/) | Node.js server framework | 4.13.0 |
| [Socket.io]() | Used for realtime communication between clients and server | 1.4.0 |
| [Webpack](https://webpack.github.io/) | Module bundling + build for client | 1.13.0 |
| [Superagent](https://github.com/visionmedia/superagent) | Universal http requests | 1.8.0 |
| [Stylus](http://stylus-lang.com/) | Expressive, dynamic, robust CSS | 0.54.0 |
