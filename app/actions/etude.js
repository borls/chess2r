import { API, ActionType } from '../constants/etude';
import { CALL_API } from 'redux-api-middleware';


export const listEtude = () => ({
  [CALL_API]: {
    endpoint: API.list,
    method: 'GET',
    types: ActionType.LIST_ETUDE
  }
});

export const createEtude = (params) => ({
  [CALL_API]: {
    endpoint: API.create,
    method: 'POST',
    body: params,
    params,
    types: ActionType.CREATE_ETUDE
  }
});

export const singleEtude = (id) => ({
  [CALL_API]: {
    endpoint: API.single(id),
    method: 'GET',
    types: ActionType.SINGLE_ETUDE
  }
});

export const updateEtude = (id, params) => ({
  [CALL_API]: {
    endpoint: API.update(id),
    method: 'POST',
    body: params,
    params,
    types: ActionType.UPDATE_ETUDE
  }
});

export const deleteEtude = (id) => ({
  [CALL_API]: {
    endpoint: API.delete(id),
    method: 'DELETE',
    params: { id },
    types: ActionType.DELETE_ETUDE
  }
});
