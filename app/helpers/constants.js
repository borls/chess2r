/**
 * generate API and ActionTypes constants
 *
 * USAGE:
 *
 * export const apiConstants(
 *     'event', // name
 *     'group/event', // root
 *     {
 *        // 'list', 'create', 'update', 'delete' - default methods
 *        format: 'format/list',
 *        import: (id) => `import/${id}`
 *     }
 * )
 * // Generate this code { API, ActionTypes }, where
 * API = {
 *    list: '/group/event/list',
 *    create: '/group/event/item',
 *    delete: (id) => `/group/event/item/${id}`,
 *    update: (id) => `/group/event/item/${id}`,
 *    format: '/group/event/format/list',
 *    import: (id) => `/group/event/import/${id}`
 * };
 * ActionType = {
 *     LIST_EVENT_REQUEST: 'LIST_EVENT_REQUEST',
 *     LIST_EVENT_SUCCESS: 'LIST_EVENT_SUCCESS',
 *     LIST_EVENT_FAILURE: 'LIST_EVENT_FAILURE',
 *     ...
 *     IMPORT_EVENT_REQUEST: 'IMPORT_EVENT_REQUEST',
 *     IMPORT_EVENT_SUCCESS: 'IMPORT_EVENT_SUCCESS',
 *     IMPORT_EVENT_FAILURE: 'IMPORT_EVENT_FAILURE',
 *     });
 * ]);
 */

export default function apiConstants (name, root, methods = {}) {
    // default methods list, create, update, delete
    if (typeof root !== 'string') throw new Error('names in MakeActionTypes should be string or array');
    const defaultMethods = {
        list: `${root}/list`,
        create: `${root}/item`,
        single: (id) => `${root}/item/${id}`, // GET
        update: (id) => `${root}/item/${id}`, // POST
        delete: (id) => `${root}/item/${id}` // DELETE
    };

    // custom methods and redefined
    const customMethods = Object.keys(methods).reduce((res, m) => {
        const method = methods[m];
        let rootMethod;
        if (typeof method === 'string') rootMethod = `${root}/${method}`;
        else if (typeof method === 'function') rootMethod = (...args) => `${root}/${method.apply(null, args)}`;
        else throw new Error(`method ${m} must to be a String or a Function`);

        return Object.assign({}, res, { [m]: rootMethod });
    }, {});

    const API = Object.assign({}, defaultMethods, customMethods);

    const ActionType = Object.keys(API).reduce((types, method) => {
        const ACTION = `${method}_${name}`.toUpperCase();
        const TYPES = [ `${ACTION}_REQUEST`, `${ACTION}_SUCCESS`, `${ACTION}_FAILURE` ];
        return Object.assign({},
            types,
            { [ACTION]: TYPES },
            TYPES.reduce((res, type) => Object.assign({}, res, { [type]: type }), {}));
    }, {});

    return { API, ActionType };
}
