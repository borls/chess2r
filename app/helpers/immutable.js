import Immutable, { OrderedMap } from 'immutable';

export const toJS = (i) => Immutable.Iterable.isIterable(i) && i.toJS() || i;

export const OrderedMapById = (arr, cb = (item) => item) => OrderedMap().withMutations(map => {
  arr.forEach(item => map.set(item.id, cb(item)));
});