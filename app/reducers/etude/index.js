import { Map, OrderedMap } from 'immutable';
import { OrderedMapById } from '../../helpers/immutable';
import { ActionType } from '../../constants/etude';
import EtudeItemRecord from './records';


const initialState = Map({
  list: OrderedMap(),
  loading: false
});

export default (state = initialState, action) => {
  switch (action.type) {

    /**
     * LIST_ETUDE
     */
    case ActionType.LIST_ETUDE_REQUEST:
      return state
        .set('loading', true);

    case ActionType.LIST_ETUDE_SUCCESS:
      return state
        .set('list', OrderedMapById(action.payload.result, (item) => new EtudeItemRecord(item)))
        .set('loading', false);

    case ActionType.LIST_ETUDE_FAILURE:
      return state
        .set('loading', true);

    /**
     * CREATE_ETUDE
     */
    case ActionType.CREATE_ETUDE_REQUEST:
      return state
        .set('loading', true);

    case ActionType.CREATE_ETUDE_SUCCESS:
      console.log(action);
      return state
        .setIn([ 'list', action.payload.result[0].id ], new EtudeItemRecord(action.payload.result[0]))
        .set('loading', false);

    case ActionType.CREATE_ETUDE_FAILURE:
      return state
        .set('loading', true);

    /**
     * UPDATE_ETUDE
     */
    case ActionType.UPDATE_ETUDE_REQUEST:
      return state
        .set('loading', true);

    case ActionType.UPDATE_ETUDE_SUCCESS:
      return state
        .updateIn([ 'list', action.params.id ], etude => etude.setParams(action.params))
        .set('loading', false);

    case ActionType.UPDATE_ETUDE_FAILURE:
      return state
        .set('loading', true);

    /**
     * DELETE_ETUDE
     */

    case ActionType.DELETE_ETUDE_REQUEST:
      return state
        .set('loading', true);

    case ActionType.DELETE_ETUDE_SUCCESS:
      return state
        .deleteIn([ 'list', action.params.id ])
        .set('loading', false);

    case ActionType.DELETE_ETUDE_FAILURE:
      return state;

    default:
      return state
        .set('loading', true);
  }
};
