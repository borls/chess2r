import { Record } from 'immutable';

const EtudeRecord = Record({
    id: undefined,
    name: undefined,
    created: null
});

class EtudeItemRecord extends EtudeRecord {
    constructor(etude) {
        super(etude);

        Object.keys(etude).forEach(key => etude[key] && this.validate(key, etude[key]));
    }

    validateString(key, value) {
        if (typeof value !== 'string') {
            throw new Error(`${key}: ${value} - не строковое выражение`);
        }
    }
    validateBoolean(key, value) {
        if (typeof value !== 'boolean' && value !== 'false' && value !== 'true') {
            throw new Error(`${key}: ${value} - не логическое выражение`);
        }
    }
    validateNumber(key, value) {
        if (typeof value !== 'number') {
            throw new Error(`${key}: ${value} - не числовое выражение`);
        }
    }
    validate(key, value) {
        switch (key) {
            case 'id':
            case 'name':
                return this.validateString(key, value);
            case 'created':
                return this.validateNumber(key, value);
            default:
                throw new Error(`${key}: ${value} - нет для нее правила!`);
        }
    }

    setValue(data) {
        const key = Object.keys(data)[0];
        const value = Object.values(data)[0];
        if (typeof value === 'undefined') return this;
        this.validate(data);
        return this.set(key, value);
    }

    setParams({ name, created }) {
        return this
          .setValue({ name })
          .setValue({ created });
    }
}

export default EtudeItemRecord;
