import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';

import etude from './etude'

export default combineReducers({
  routing: routerReducer,
  etude
});