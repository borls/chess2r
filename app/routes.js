import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Etude from './views/page/etude';

// по идее сначала должен быть path=/ component=application
export default (
  <Route path='/' component={Etude}>
    <IndexRoute components={{ Etude: Etude }} />
    <Route path='etudes' components={{ Etude: Etude }} />
    <Route path='test-routes-etudes' components={{ Etude }} />
  </Route>
);