/* global __DEV__ */
import { createStore, applyMiddleware } from 'redux';

import middleWares from './middlewares';
import rootReducer from '../reducers';

export default (preloadedState) => {
  const store = createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(...middleWares)
  );

  if (__DEV__ && module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => store.replaceReducer(require('../reducers').default))
  }

  return store;
};
