module.exports = (typeof window === 'undefined') ?
                 require('./configureStore.server') :
                 require('./configureStore.client');
