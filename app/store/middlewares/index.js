/* global __DEV__ */
import thunk from 'redux-thunk';
import { apiMiddleware } from 'redux-api-middleware';


const logger = __DEV__ && require('./logger.js').default;
export default [thunk, apiMiddleware, logger].filter(Boolean);
