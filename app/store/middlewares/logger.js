import createLogger from 'redux-logger';
import { toJS } from 'helpers/immutable'


export default createLogger({
  stateTransformer: (state) => Object.keys(state)
    .reduce((newState, i) => Object.assign({}, newState, { [i]: toJS(state[i]) }), {}),
  colors: {
    title: ({ type }) => ({
      REQUEST: '#eeeeee',
      SUCCESS: '#d6e9c6',
      FAILURE: '#c7254e',
      _CHANGE: '#f7f7f9'
    }[type && type.slice(-7)]),
    prevState: (prevState) => '#eeeeee',
    action: (action) => '#337ab7',
    nextState: (nextState) => '#3c763d',
    error: (error, prevState) => '#c7254e'
  },
  collapsed: true,
})
