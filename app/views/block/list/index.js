import React, { Component, PropTypes } from 'react'

class List extends Component {
  render() {
    const { list } = this.props;
    return (
      <ui className="chs-list">
        {
          Object.keys(list).map(item => <ul key={item}>{list[item].name}</ul>)
        }
      </ui>
    )
  }
}

List.propTypes = {
  list: PropTypes.object.isRequired,
};

export default List;
