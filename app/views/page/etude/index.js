import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Template from './template'
import * as Actions from '../../../actions/etude';


const mapStateToProps = ({ etude }) => ({
  list: etude.get('list').toJS(),
  loading: etude.get('loading')
});

const mapDispatchToProps = (dispatch) => bindActionCreators(Actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Template);
