import React, { Component, PropTypes } from 'react'
import { List } from './../../block';


class Etude extends Component {
  constructor(props) {
    super(props);

    props.listEtude();
  }
  render() {
    const { list, loading } = this.props;
    return (
      <List
        className="etude__list"
        list={list}
      />
    )
  }
}

Etude.propTypes = {
  listEtude: PropTypes.func.isRequired,
  // list: PropTypes.object.isRequired
};

export default Etude;
