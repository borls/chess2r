import 'babel-polyfill'
import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import routes from '../app/routes';

import { Provider } from 'react-redux';
import configureStore from '../app/store';
import { fromJS } from 'immutable';

const preloadedState = window.__PRELOADED_STATE__;

// convert plain js object to immutable
const store = configureStore(
  Object.keys(preloadedState)
    .reduce((state, reducer) => ({ ...state, [reducer]: fromJS(preloadedState[reducer]) }), {})
);

const history = syncHistoryWithStore(browserHistory, store);
render(
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>,
  document.getElementById('root')
);

// // Now that we have rendered...
// setupRealtime(store, actions);
//
// // lets mutate state and set UserID as key from local storage
// store.dispatch(actions.setUserId(getOrSetUserId()));