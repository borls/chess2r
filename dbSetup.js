/* global Promise */
import r from 'rethinkdb';
import config from 'config';


const rethinkdb = config.get('rethinkdb');
let DATABASE = rethinkdb.db;
let TABLES = ['etude'];

r.connect(rethinkdb)
.then(conn => {
  console.log(' [-] Database Setup');
  return createDbIfNotExists(conn)
  .then(() => Promise.all(TABLES.map((table) => createTableIfNotExists(conn, table))))
  .then(() => Promise.all(TABLES.map((table) => fillTableIfHaveData(conn, table))))
  .then(() => closeConnection(conn));
});


const createDbIfNotExists = (conn) => getDbList(conn)
  .then((list) => {
    if(~list.indexOf(DATABASE)) {
      console.log(' [!] Database already exists:', DATABASE);
      return Promise.resolve(true);
    }

    return createDatabase(conn);
  });

const createTableIfNotExists = (conn, table) => getTableList(conn)
  .then((list) => {
    if(~list.indexOf(table)) {
      console.log(' [!] Table already exists:', table);
      return Promise.resolve(true);
    }

    return createTable(conn, table);
  });

// auxiliary functions
const getDbList = (conn) => r.dbList().run(conn);
const getTableList = (conn) => r.db(DATABASE).tableList().run(conn);
const createDatabase = (conn) => {
  console.log(' [-] Create Database:', DATABASE);
  return r.dbCreate(DATABASE).run(conn);
};
const createTable = (conn, table) => {
  console.log(' [-] Create Table:', table);
  return r.db(DATABASE).tableCreate(table).run(conn);
};
const closeConnection = (conn) => {
  console.log(' [x] Close connection!');
  return conn.close();
};

const fillTableIfHaveData = (conn, table) => {
  const path = `./data/${table}/list.json`;

  try {
    require.resolve(path);
  } catch(e) {
    console.error(` [!] ${table}: list.json is not found`);
    return Promise.resolve(true);
  }

  const data = require(path);

  const preview = JSON.stringify(data);
  console.log(`  [ ${data.length} ] ${table}: ${(preview.length < 100) && preview || `${preview.slice(0, 100)} ...`}`);
  return r.db(DATABASE).table(table).insert(data).run(conn);
};
