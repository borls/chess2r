import express from 'express';
import * as middleware from './middlewares';
import bodyParser from 'body-parser';
import routes from './routes';

export default express()
  .use(middleware.api)
  .use(middleware.cors)
  // .use(require('serve-static')(path.join(__dirname, config.get('buildDirectory'))))
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(routes);
