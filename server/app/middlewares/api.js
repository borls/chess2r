// var _isArray = require('lodash.isarray');
import isArray from 'lodash.isarray'


export default function (req, res, next) {
  res.api = (result = [], errorCode = 0, errorMessage = '') =>
    res.json({
      errorCode,
      errorMessage,
      result: isArray(result) && result || [ result ]
    });
  next();
};
