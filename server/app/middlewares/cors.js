export default function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Location, WWW-Authenticate, Authorization');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Expose-Headers', 'Authorization, Role');
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,OPTION,DELETE');
  res.header('Origin', '*');
  next();
};