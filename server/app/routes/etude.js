import Query from '../../api/query';

export default function registerGroups(router) {
  const etude = new Query({ table: 'etude' });
  router.get('/api/0/list', etude.getList);
  router.post('/api/0/item', etude.createItem);
  router.get('/api/0/item/:id', etude.getItem);
  router.post('/api/0/item/:id', etude.updateItem);
  router.delete('/api/0/item/:id', etude.removeItem);
};
