import registerEtude from './etude';
import path from 'path'
export {
  registerEtude,
};

import express from 'express';
const router = express.Router();

registerEtude(router);

router.get('/favicon.ico', (req, res) => res.sendFile(path.join(__dirname, '..', '..',  '..', 'assets', 'images', 'favicon.ico')));

export default router;