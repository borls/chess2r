import { Map } from 'immutable';
import { OrderedMapById } from '../app/helpers/immutable';
import app from './app';
import webpack from 'webpack';
import webpackConfig from '../webpack.config';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import configureStore from '../app/store';
import config from 'config';
import Service from './api/service';
import { RouterContext, match } from 'react-router';

import routes from '../app/routes';

const port = config.get('express.port');

// Use this middleware to set up hot module reloading via webpack.
const compiler = webpack(webpackConfig);
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: webpackConfig.output.publicPath }));
app.use(webpackHotMiddleware(compiler));

const renderFullPage = (html, preloadedState) =>
  `
  <!doctype html>
  <html>
    <head>
      <title>Redux Universal Example</title>
      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    </head>
    <body>
      <div id="root">${html}</div>
      <script>
        window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\x3c')}
      </script>
      <script src="/static/bundle.js"></script>
    </body>
  </html>
  `;

const handleRender = (req, res) => {
  console.log(' [x] Request for', req.url);
  const etude = new Service({ table: 'etude' });

  etude.getList().then(initialList => {
    let preloadedState = { etude: Map({ list: OrderedMapById(initialList), loading: false }) };

    const store = configureStore(preloadedState);

    // Wire up routing based upon routes
    match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
      console.log(error, redirectLocation, renderProps);
      if (error || !renderProps) {

        console.log(111, routes);
        console.log(222, req.url);
        console.log(333, error, redirectLocation, renderProps);
        if (req.url === '/bundle.js') {
          console.log(' | Hold up, are you sure you are hitting the app at http://localhost:3001?');
          console.log(' | On development bundle.js is served by the Webpack Dev Server and so you need to hit the app on port 3001, not port 3000.');
        }
        console.log(error || 'Error: No matching universal route found');

        res.status(400);
        res.send(error || 'Error: No matching universal route found');
        return;
      }
      if (redirectLocation) {
        res.redirect(redirectLocation);
        return;
      }

      // Render the component to a string
      const html = renderToString(
        <Provider store={store}>
          <RouterContext {...renderProps} />
        </Provider>
      );

      // Send the rendered page back to the client with the initial state

      // res.render('index', {
      //   html,
      //   production: process.env.NODE_ENV === 'production',
      //   preloadedState: JSON.stringify(store.getState())
      // });
      res.send(renderFullPage(html, store.getState()));
    });
  });
};



// This is fired every time the server side receives a request
app.use(handleRender);

app.listen(port, error =>
  error ?
  console.error(error) :
  console.info(`==> 🌎  Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`)
);
